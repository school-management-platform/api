<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use Validator;

class CourseController extends Controller
{
    public function list(Request $request) {
        $courses = Course::all();

        return $this->_res([
            'code' => 200,
            'data' => $courses
        ]);
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $course = new Course;
        $course->name = $request->name;
        $course->school_id = $request->user()->school_id;
        $course->save();

        return $this->_res([
            'code' => 200,
            'data' => $course
        ]);
    }

    public function update(Request $request) {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'id' => 'required|exists:courses,id'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $course = Course::findOrFail($request->id);
        $course->name = $request->name;
        $course->save();

        return $this->_res([
            'code' => 200,
            'data' => $course
        ]);
    }

    public function delete(Request $request) {
        $v = Validator::make($request->all(), [
            'id' => 'required|exists:courses,id'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $course = Course::findOrFail($request->id);
        $course->delete();

        return $this->_res([
            'code' => 200,
            'data' => $course
        ]);
    }
}
