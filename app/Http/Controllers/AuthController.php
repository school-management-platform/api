<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Validator;

use App\Models\User;
use App\Models\School;

class AuthController extends Controller
{
    public function login(Request $request) {
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'role' => 'required',
            'device_name' => 'nullable',
            'school_id' => 'required|exists:schools,id'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $user = User::where('email', $request->email)
            ->where('school_id', $request->school_id)   
            ->where('role', $request->role)   
            ->where('status', 1)   
            ->with('school');

        if($request->role == 2) {
            $user = $user->with('teacher');
        }

        if($request->role == 3) {
            $user = $user->with('student');
        }

        $user = $user->first();

        if($user
        && Hash::check($request->password, $user->password)) {
            
            $token = $user->createToken($request->device_name ?? 'web', explode(',', $user->grant))->plainTextToken;

            $user->_token = [
                'prefix' => 'Bearer',
                'token' => $token
            ];
            
            return $this->_res([
                'code' => 200,
                'data' => $user
            ]);
        }

        return $this->_res([
            'code' => 422,
            'data' => [],
            'errors' => [
                'password' => [
                    'Invalid email and password combination'
                ]
            ]
        ]);

    }
    public function admin(Request $request) {
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'role' => 'required|in:0',
            'device_name' => 'nullable',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $user = User::where('email', $request->email) 
            ->where('role', $request->role)
            ->first();

        if($user
        && Hash::check($request->password, $user->password)) {
            
            $token = $user->createToken($request->device_name ?? 'web', explode(',', $user->grant))->plainTextToken;

            $user->_token = [
                'prefix' => 'Bearer',
                'token' => $token
            ];

            $user->schools = School::all();
            
            return $this->_res([
                'code' => 200,
                'data' => $user
            ]);
        }

        return $this->_res([
            'code' => 422,
            'data' => [],
            'errors' => [
                'password' => [
                    'Invalid email and password combination'
                ]
            ]
        ]);
    }

    public function csrf(Request $request) {
        $token = csrf_token();

        return $this->_res([
            'code' => 200,
            'data' => [
                'token' => $token
            ]
        ]);
    }
}
