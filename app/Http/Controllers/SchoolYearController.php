<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Models\SchoolYear;
class SchoolYearController extends Controller
{
    public function current(Request $request) {
        $v = Validator::make($request->all(), [
            'school_id' => 'required|exists:schools,id',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $sy = SchoolYear::where('school_id', $request->school_id)
            ->orderBy('id', 'DESC')
            ->first();

        return $this->_res([
            'code' => 200,
            'data' => $sy
        ]);
    }

    public function list(Request $request) {
        $sy = SchoolYear::where('school_id', $request->user()->school_id)
            ->get();

        return $this->_res([
            'code' => 200,
            'data' => $sy
        ]);
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        $years = explode('-', $request->name);

        $sy = new SchoolYear;
        $sy->name = $request->name;
        $sy->start_year = $years[0];
        $sy->end_year = $years[1];
        $sy->school_id = $request->user()->school_id;
        $sy->save();

        return $this->_res([
            'code' => 200,
            'data' => $sy
        ]);
    }

    public function update(Request $request) {
        $v = Validator::make($request->all(), [
            'school_year_id' => 'required|exists:school_years,id',
            'name' => 'required'
        ]);

        $years = explode('-', $request->name);

        $sy = SchoolYear::find($request->school_year_id);
        $sy->name = $request->name;
        $sy->start_year = $years[0];
        $sy->end_year = $years[1];
        $sy->save();

        return $this->_res([
            'code' => 200,
            'data' => $sy
        ]);
    }
}
