<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Models\EducationalLevel;

class EducationalLevelController extends Controller
{
    public function list(Request $request) {
        $el = EducationalLevel::all();

        return $this->_res([
            'code' => 200,
            'data' => $el
        ]);
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
            'numeric' => 'required|unique:educational_levels,numeric',
            'name' => 'required'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $el = new EducationalLevel;
        $el->numeric = $request->numeric;
        $el->name = $request->name;
        $el->school_id = $request->user()->school_id;
        $el->save();

        return $this->_res([
            'code' => 200,
            'data' => $el,
        ]);
    }

    public function update(Request $request) {
        $v = Validator::make($request->all(), [
            'educational_level_id' => 'required|exists:educational_levels,id',
            'numeric' => 'required|unique:educational_levels,numeric,' . $request->educational_level_id,
            'name' => 'required'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $el = EducationalLevel::findOrFail($request->educational_level_id);
        $el->numeric = $request->numeric;
        $el->name = $request->name;
        $el->save();

        return $this->_res([
            'code' => 200,
            'data' => $el,
        ]);
    }
}
