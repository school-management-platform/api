<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Models\Subject;

class SubjectController extends Controller
{
    public function list(Request $request) {
        $subjects = Subject::with('grade_level')
                ->with('grade_level.educational_level')
                ->where('school_id', $request->user()->school_id);
            
        if($request->grade_level_id !== null)
            $subjects = $subjects->where('grade_level_id', $request->grade_level_id);

        $subjects = $subjects->get();

        return $this->_res([
            'code' => 200,
            'data' => $subjects
        ]);
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'grade_level_id' => 'required|exists:grade_levels,id',
            'color' => 'nullable'
        ]);
        
        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $sub = new Subject;
        $sub->name = $request->name;
        $sub->grade_level_id = $request->grade_level_id;
        $sub->color = $request->color;
        $sub->school_id = $request->user()->school_id;
        $sub->save();

        return $this->_res([
            'code' => 200,
            'data' => $sub
        ]);
    }

    public function update(Request $request) {
        $v = Validator::make($request->all(), [
            'subject_id' => 'required|exists:subjects,id',
            'name' => 'required',
            'grade_level_id' => 'required|exists:grade_levels,id',
            'color' => 'nullable'
        ]);
        
        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $sub = Subject::findOrFail($request->subject_id);
        $sub->name = $request->name;
        $sub->grade_level_id = $request->grade_level_id;
        $sub->color = $request->color;
        $sub->save();

        return $this->_res([
            'code' => 200,
            'data' => $sub
        ]);
    }

    public function status(Request $request) {
        $v = Validator::make($request->all(), [
            'subject_id' => 'required|exists:subjects,id',
            'status' => 'required|in:1,0',
        ]);
        
        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $sub = Subject::findOrFail($request->subject_id);
        $sub->status = $request->status;
        $sub->save();

        return $this->_res([
            'code' => 200,
            'data' => $sub
        ]);
    }
}
