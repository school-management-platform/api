<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Models\Department;

class DepartmentController extends Controller
{
    public function list(Request $request) {
        $dep = Department::where('school_id', $request->user()->school_id)
            ->get();

        return $this->_res([
            'code' => 200,
            'data' => $dep
        ]);
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
            'name' => 'required|unique:departments,name'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $dep = new Department;
        $dep->name = $request->name;
        $dep->school_id = $request->user()->id;
        $dep->save();

        return $this->_res([
            'code' => 200,
            'data' => $dep
        ]);
    }

    public function update(Request $request) {
        $v = Validator::make($request->all(), [
            'department_id' => 'required|exists:departments,id',
            'name' => 'required|unique:departments,name,' . $request->department_id
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $dep = Department::findOrFail($request->department_id);
        $dep->name = $request->name;
        $dep->save();

        return $this->_res([
            'code' => 200,
            'data' => $dep
        ]);
    }
}
