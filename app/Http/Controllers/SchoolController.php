<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;

use App\Models\School;

class SchoolController extends Controller
{
    public function list(Request $request) {
        $schools = null;
        
        if($request->status == null)
            $schools = School::where('status', 1)->get();
            
        if($request->status !== null && $request->status == 0)
            $schools = School::where('status', 0)->get();

        if($request->status !== null && $request->status == 1)
            $schools = School::where('status', 1)->get();

        if($request->status !== null && $request->status == 3)
            $schools = School::all();

        return $this->_res([
            'code' => 200,
            'data' => $schools
        ]);
    }

    public function get(Request $request) {
        $v = Validator::make($request->all(), [
            'id' => 'required|exists:schools,id'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $school = School::findOrFail($request->id);

        return $this->_res([
            'code' => 200,
            'data' => $school
        ]);
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required|unique:schools,code',
            'logo' => 'required|mimetypes:image/jpeg,image/png,image/jpg',
            'address' => 'required'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $ext = $request->logo->extension();
        $name = $request->logo->getClientOriginalName();
        $file_name = md5($name . time()) .'.'.$ext;
        $request->logo->storeAs('logo', $file_name);

        $school = new School;
        $school->name = $request->name;
        $school->code = $request->code;
        $school->logo = '/logo/' . $file_name;
        $school->address = $request->address;
        $school->save();

        return $this->_res([
            'code' => 200,
            'data' => $school,
        ]);
    }

    

    public function update(Request $request) {
        $v = Validator::make($request->all(), [
            'school_id' => 'required|exists:schools,id',
            'name' => 'required',
            'code' => 'required|unique:schools,code,' . $request->school_id,
            // 'logo' => 'nullable|sometimes|image|mimetypes:image/jpeg,image/png,video/mp4',
            'address' => 'required'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $school = School::find($request->school_id);
        $school->name = $request->name;
        $school->code = $request->code;
        $school->address = $request->address;

        if($request->hasFile('logo')) {
            $ext = $request->logo->extension();
            $name = $request->logo->getClientOriginalName();
            $file_name = md5($name . time()) .'.'.$ext;
            $request->logo->storeAs('logo', $file_name);

            $school->logo = '/logo/' . $file_name;
        }

        $school->save();

        return $this->_res([
            'code' => 200,
            'data' => $school,
        ]);
    }

    public function status(Request $request) {
        $v = Validator::make($request->all(), [
            'school_id' => 'required|exists:schools,id',
            'status' => 'required|in:1,0',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $school = School::find($request->school_id);
        $school->status = $request->status;
        $school->save();

        return $this->_res([
            'code' => 200,
            'data' => $school,
        ]);
    }
}
