<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use Validator;

use App\Models\Section;
use App\Models\SectionClass;
use App\Models\SchoolYear;
use App\Models\StudentClass;
use App\Models\SectionClassesMeeting;

class SectionController extends Controller
{

    public function list(Request $request) {
        $sections = Section::with('grade_level')
                ->with('grade_level.educational_level')
                ->with('user')
                ->with('user.teacher')
                ->with('classes')
                ->with('classes.subject')
                ->with('classes.user')
                ->with('classes.user.teacher');

        if(in_array($request->user()->role, [1,2,3]))
            $sections = $sections->where('school_id', $request->user()->school_id);

        if($request->educational_level_id != null)
            $sections = $sections->where('educational_level_id', $request->educational_level_id);

        if($request->grade_level_id != null)
            $sections = $sections->where('grade_level_id', $request->grade_level_id);

        if($request->user_id != null)
            $sections = $sections->where('user_id', $request->user_id);

        if($request->school_id != null && !in_array($request->user()->role, [1,2,3]))
            $sections = $sections->where('school_id', $request->user_id);

        if($request->paginate !== null
        && $request->paginate == 'false')
            $sections = $sections->get();
        else
            $sections = $sections->paginate(10);

        return $this->_res([
            'code' => 200,
            'data' => $sections
        ]);
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'user_id' => 'required|exists:users,id',
            'grade_level_id' => 'required|exists:grade_levels,id',
            'educational_level_id' => 'required|exists:educational_levels,id',
            'classes.*.subject_id' => 'required|exists:subjects,id',
            'classes.*.user_id' => 'required|exists:users,id',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        try {
            return DB::transaction(function () use ($request) {
                $section = new Section;
                $section->name = $request->name;
                $section->user_id = $request->user_id;
                $section->school_id = $request->user()->school_id;
                $section->school_year_id  = SchoolYear::orderBy('created_at', 'DESC')->first()->id;
                $section->grade_level_id = $request->grade_level_id;
                $section->educational_level_id = $request->educational_level_id;
                $section->save();

                $classes = [];
                foreach ($request->classes as $key => $value) {
                    $classes[] = SectionClass::create([
                        "user_id" => $value['user_id'],
                        "subject_id" => $value['subject_id'],
                        "section_id" => $section->id
                    ]);
                }

                return $this->_res([
                    'code' => 200,
                    'data' => [
                        'section' => $section,
                        'classes' => $classes
                    ],
                ]);
            });
        } catch(Exception $e) {
            return $this->_res([
                'code' => 500,
                'data' => [],
                'errors' => [
                    'name' => [
                        'Something went wrong.'
                    ]
                ] 
            ]);
        }
    }

    public function classes(Request $request) {
        $classes = SectionClass::with('user')
                ->with('user.teacher')
                ->with('subject')
                ->whereHas('section', function(Builder $q) use ($request) {
                        $q->where('school_id', $request->user()->school_id);

                        if($request->educational_level_id !== null)
                            $q->where('educational_level_id', $request->educational_level_id);

                        if($request->grade_level_id !== null)
                            $q->where('grade_level_id', $request->grade_level_id);

                        if($request->user_id !== null)
                            $q->where('user_id', $request->user_id);
                    }
                )
                ->with('section.grade_level')
                ->with('section.educational_level')
                ->with('section.user')
                ->get();
        
        return $this->_res([
            'code' => 200,
            'data' => $classes
        ]);
    }
}
