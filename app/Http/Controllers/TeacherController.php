<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use Storage;

use App\Models\Teacher;
use App\Models\User;

class TeacherController extends Controller
{
    public function list(Request $request) {

        // if(!$request->user()->tokenCan('teacher:list'))
        //     return $this->_res([
        //         'code' => 401,
        //         'data' => [],
        //         'msg' => 'You do not have a permission executing this action.'
        //     ]);

        $user = User::where('role', 2)
            ->has('teacher')
            ->with('teacher')
            ->with('teacher.advisory')
            ->where('school_id', $request->user()->school_id);

        if($request->status !== null)
            $user = $user->where('status', $request->status);

        if($request->paginate !== null
        && $request->paginate == 'false')
            $user = $user->get();
        else
            $user = $user->paginate(10);

        return $this->_res([
            'code' => 200,
            'data' => $user,
        ]);

    }

    public function get(Request $request) {
        $v = Validator::make($request->all(), [
            'user_id' => 'required|exists:teachers,user_id'
        ]);

        if($v->fails()) 
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $user = User::where('role', 2)
            ->where('id', $request->user_id)
            ->with('teacher')
            ->with('teacher.department')
            ->with(['teacher.advisory' => function($q) {
                $q->where('status', 1);
                $q->with('educational_level');
                $q->with('grade_level');
            }])
            ->with('teacher.classes')
            ->with('teacher.classes.section')
            ->with('teacher.classes.section.grade_level')
            ->with('teacher.classes.subject')
            ->first();

        return $this->_res([
            'code' => 200,
            'data' => $user 
        ]);

    }

    public function store(Request $request) {

        // if(!$request->user()->tokenCan('teacher:store'))
        //     return $this->_res([
        //         'code' => 401,
        //         'data' => [],
        //         'msg' => 'You do not have a permission executing this action.'
        //     ]);

        $v = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'gender' => 'required',
            'hired_date' => 'date',
            'department_id' => 'nullable|exists:departments,id'
        ]);

        if($v->fails()) 
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $filename = '';

        if($request->hasFile('picture')) {
            $ext = $request->picture->extension();
            $name = $request->picture->getClientOriginalName();
            $filename  = md5($name . time()) .'.'.$ext;
            $request->picture->storeAs('avatars', $filename);
        }

        
        try {
            $data = DB::transaction(function() use ($request, $filename) {
                $user = new User;
                $user->firstname = $request->firstname;
                $user->avatar = $filename == '' ? '' : '/avatars/' . $filename;
                $user->middlename = $request->middlename;
                $user->lastname = $request->lastname;
                $user->email = $request->email;
                $user->password = bcrypt('password');
                $user->school_id = $request->user()->school_id;
                $user->role = 2;
                $user->status = 1;
                $user->save();

                $teacher = new Teacher;
                $teacher->user_id = $user->id;
                $teacher->gender = $request->gender;
                $teacher->hired_date = $request->hired_date;
                $teacher->birthday = $request->birthday;
                // $teacher->department_id = $request->department_id || 1
                $teacher->save();

                return [
                    'user' => $user,
                    'teacher' => $teacher
                ];
            });

            return $this->_res([
                'code' => 200,
                'data' => $data
            ]);
        } catch(\Exception $e) {
            Storage::delete('/avatars/' . $filename);
            throw $e;
        }
    }   

    public function update(Request $request) {

        // if(!$request->user()->tokenCan('teacher:update'))
        //     return $this->_res([
        //         'code' => 401,
        //         'data' => [],
        //         'msg' => 'You do not have a permission executing this action.'
        //     ]);

        $v = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email,' . $request->user_id,
            'gender' => 'required',
            'hired_date' => 'date',
            'department_id' => 'nullable|exists:departments,id'
        ]);

        if($v->fails()) 
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $user = User::where('id', $request->user_id)->where('role', 2)->first();
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role = 2;
        $user->save();

        $teacher = Teacher::where('user_id', $request->user_id)->first();
        $teacher->user_id = $user->id;
        $teacher->gender = $request->gender;
        $teacher->hired_date = $request->hired_date;
        $teacher->birthday = $request->birthday;
        $teacher->department_id = $request->department_id;
        $teacher->save();

        return $this->_res([
            'code' => 200,
            'data' => [
                'user' => $user,
                'teacher' => $teacher
            ]
        ]);

    }
}
