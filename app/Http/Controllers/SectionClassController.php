<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Validator;

use App\Models\SectionClass;
use App\Models\StudentClass;
use App\Models\SectionClassesMeeting;
use App\Models\SectionClassesModule;
use App\Models\SectionClassesAnnouncement;
use App\Models\SectionClassesAssessment;
use App\Models\SectionClassesGoogleLink;
use App\Models\SectionClassesSchedule;
use App\Models\SectionClassesDiscussion;

class SectionClassController extends Controller
{
    public function get_class(Request $request) {
        $v = Validator::make($request->all(), [
            'class_id' => 'required|exists:section_classes,id',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $class = SectionClass::with('user')
                ->with('user.teacher')
                ->with('subject')
                ->with([
                    'announcement' => function($q) {
                        // $q->where('type', 1);
                    } 
                ])
                ->with('discussion')
                ->with('meeting')
                ->with('module')
                ->with('section')
                ->with('google_link')
                ->with('schedule')
                ->with('section.grade_level')
                ->where('id', $request->class_id)
                ->first();

        return $this->_res([
            'code' => 200,
            'data' => $class
        ]); 
    }

    public function get_class_students(Request $request) {
        $v = Validator::make($request->all(), [
            'class_id' => 'required|exists:section_classes,id',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $students = StudentClass::where('section_class_id', $request->class_id)
                ->with('user')
                ->with('user.student')
                ->get();

        return $this->_res([
            'code' => 200,
            'data' => $students
        ]); 
    }
    

    // Start Meeting
        public function get_class_meetings(Request $request) {
            $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);

            $meetings = SectionClassesMeeting::where('section_class_id', $request->class_id)
                    ->orderBy('created_at', 'DESC')
                    ->get();

            return $this->_res([
                'code' => 200,
                'data' => $meetings
            ]); 
        } 

        public function store_class_meeting(Request $request) {
            $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
                'name' => 'required',
                'link' => 'required',
                'date' => 'required',
                'time' => 'required',
                'platform' => 'required',
                'password' => 'nullable'
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);


            $meeting = new SectionClassesMeeting;
            $meeting->section_class_id = $request->class_id;
            $meeting->name = $request->name;
            $meeting->date = $request->date;
            $meeting->time = $request->time;
            $meeting->platform = $request->platform;
            $meeting->password = $request->password;
            $meeting->link = $request->link;
            $meeting->save();

            return $this->_res([
                'code' => 200,
                'data' => $meeting
            ]);
        }

        public function update_class_meeting(Request $request) {
            $v = Validator::make($request->all(), [
                'meeting_id' => 'required|exists:section_classes_meetings,id',
                'name' => 'required',
                'date' => 'required',
                'time' => 'required',
                'platform' => 'required',
                'password' => 'nullable'
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);


            $meeting = SectionClassesMeeting::findOrFail($request->meeting_id);
            $meeting->name = $request->name;
            $meeting->date = $request->date;
            $meeting->time = $request->time;
            $meeting->platform = $request->platform;
            $meeting->password = $request->password;
            $meeting->save();

            return $this->_res([
                'code' => 200,
                'data' => $meeting
            ]);
        }

        public function delete_class_meeting(Request $request) {
             $v = Validator::make($request->all(), [
                'meeting_id' => 'required|exists:section_classes_meetings,id',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);


            $meeting = SectionClassesMeeting::find($request->meeting_id);
            $meeting->delete();

            return $this->_res([
                'code' => 200,
                'data' => $meeting
            ]);
        }

    // End Meeting

    // Announcements
        public function store_class_announcement(Request $request) {
            $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
                'content' => 'required',
                'type' => 'required'
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);

            $announcement = SectionClassesAnnouncement::where('section_class_id', $request->class_id)->first();

            if(!$announcement) {
                $announcement = new SectionClassesAnnouncement;
            }

            $announcement->section_class_id = $request->class_id;
            $announcement->content = $request->content;
            $announcement->type = $request->type;
            $announcement->user_id = $request->user()->id;
            $announcement->save();

            return $this->_res([
                'code' => 200,
                'data' => $announcement
            ]);
        }
    // End Annoucements

    // Modules
        public function get_class_module(Request $request) {
            $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
                'type' => 'required',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);

            $module = SectionClassesModule::where('section_class_id', $request->class_id)
                    ->where('type', $request->type)
                    ->orderBy('created_at', 'DESC')
                    ->get();

            return $this->_res([
                'code' => 200,
                'data' => $module
            ]);
        }

        public function store_class_module(Request $request) {
            $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
                'type' => 'required',
                'name' => 'required',
                'file' => 'required|mimes:pdf,docx'
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);

            $ext = $request->file->extension();
            $name = $request->file->getClientOriginalName();
            $filename  = md5($name . time()) .'.'.$ext;
            $request->file->storeAs('files', $filename);

            $module = new SectionClassesModule;
            $module->section_class_id = $request->class_id;
            $module->name = $request->name;
            $module->type = $request->type;
            $module->path = '/files/' . $filename;
            $module->date = $request->date;
            $module->time = $request->time;
            $module->save();

            return $this->_res([
                'code' => 200,
                'data' => $module
            ]);
        }

        public function delete_class_module(Request $request) {
            $v = Validator::make($request->all(), [
                'module_id' => 'required|exists:section_classes_modules,id',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);

            $module = SectionClassesModule::find($request->module_id);
            Storage::delete($module->path);
            $module->delete();

            return $this->_res([
                'code' => 200,
                'data' => $module
            ]);
        }
    // End Modules

    // Start Assessment
        public function get_class_assessments(Request $request) {
            $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);

            $assessments = SectionClassesAssessment::where('section_class_id', $request->class_id)
                    ->get();

            return $this->_res([
                'code' => 200,
                'data' => $assessments
            ]); 
        } 

        public function store_class_assessment(Request $request) {
            $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
                'name' => 'required',
                'link' => 'required',
                'date' => 'required',
                'time' => 'required',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);


            $assessment = new SectionClassesAssessment;
            $assessment->section_class_id = $request->class_id;
            $assessment->name = $request->name;
            $assessment->date = $request->date;
            $assessment->time = $request->time;
            $assessment->link = $request->link;
            $assessment->save();

            return $this->_res([
                'code' => 200,
                'data' => $assessment
            ]);
        }

        public function update_class_assessment(Request $request) {
            $v = Validator::make($request->all(), [
                'assessment_id' => 'required|exists:section_classes_meetings,id',
                'name' => 'required',
                'date' => 'required',
                'time' => 'required',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);


            $assessment = SectionClassesAssessment::findOrFail($request->assessment_id);
            $assessment->name = $request->name;
            $assessment->date = $request->date;
            $assessment->time = $request->time;
            $assessment->link = $request->link;
            $assessment->save();

            return $this->_res([
                'code' => 200,
                'data' => $assessment
            ]);
        }

        public function delete_class_assessment(Request $request) {
             $v = Validator::make($request->all(), [
                'assessment_id' => 'required|exists:section_classes_assessments,id',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);


            $assessment = SectionClassesAssessment::find($request->assessment_id);
            $assessment->delete();

            return $this->_res([
                'code' => 200,
                'data' => $assessment
            ]);
        }

    // End Assessments

    // Google Link
        public function store_class_google_link(Request $request) {
            $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
                'link' => 'required',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);

            $gl = SectionClassesGoogleLink::where('section_class_id', $request->class_id)->first();

            if(!$gl) {
                $gl = new SectionClassesGoogleLink;
            }

            $gl->section_class_id = $request->class_id;
            $gl->link = $request->link;
            $gl->save();

            return $this->_res([
                'code' => 200,
                'data' => $gl
            ]);
        }
    // End Google Link

    // Schedule
        public function store_class_schedule(Request $request) {
            $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
                'schedule' => 'required',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);

            $schedule = SectionClassesSchedule::where('section_class_id', $request->class_id)->first();

            if(!$schedule) {
                $schedule = new SectionClassesSchedule;
            }

            $schedule->section_class_id = $request->class_id;
            $schedule->schedule = $request->schedule;
            $schedule->save();

            return $this->_res([
                'code' => 200,
                'data' => $schedule
            ]);
        }
    // End Schedule

    // Discussion
        public function get_class_discussion(Request $request) {
             $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);

            $posts = SectionClassesDiscussion::where('section_class_id', $request->class_id)
                    ->where('section_classes_discussion_id', null)
                    ->with('user')
                    ->with('user.student')
                    ->with('user.teacher')
                    ->with('replies')
                    ->with('replies.user')
                    ->with('replies.user.student')
                    ->with('replies.user.teacher')
                    ->orderBy('created_at', 'DESC')
                    ->paginate(30);

            return $this->_res([
                'code' => 200,
                'data' => $posts
            ]); 
        }

        public function store_class_discussion(Request $request) {
             $v = Validator::make($request->all(), [
                'class_id' => 'required|exists:section_classes,id',
                'message' => 'required',
                'discussion_id' => 'nullable|exists:section_classes_discussions,id'
            ]);

            if($v->fails())
                return $this->_res([
                    'code' => 422,
                    'data' => [],
                    'errors' => $v->errors()
                ]);
            
            $post = new SectionClassesDiscussion;
            $post->section_class_id = $request->class_id;
            $post->message = $request->message;
            $post->section_classes_discussion_id = $request->discussion_id;
            $post->user_id = $request->user()->id;
            $post->save();

            return $this->_res([
                'code' => 200,
                'data' => $post
            ]); 
        }
    // End Discussion
}
