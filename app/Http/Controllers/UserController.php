<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Validator;

class UserController extends Controller
{
    public function status(Request $request) {
        $v = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'status' => 'required|in:1,0,2',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $user = User::find($request->user_id);
        $user->status = $request->status;
        $user->save();

        return $this->_res([
            'code' => 200,
            'data' => $user,
        ]);
    }
} 
