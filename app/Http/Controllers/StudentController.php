<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Rules\UserActive;

use App\Models\Student;
use App\Models\User;
use App\Models\StudentRecord;
use App\Models\StudentClass;
use App\Models\Rejection;
use App\Models\Section;
use App\Models\SectionClass;
use App\Models\GradeLevel;
use App\Models\SchoolYear;

class StudentController extends Controller
{

    public function list(Request $request) {
        $user = User::where('role', 3)
                ->has('student')
                ->with('reject')
                ->with('student')
                ->with('student.applying_for')
                ->with('student.applying_for.educational_level')
                ->with('student.school_year')
                ->with('student.current_record')
                ->with('student.current_record.section')
                ->with('student.current_record.grade_level')
                ->with('student.current_record.school_year')
                ->with('student.classes');

        if(in_array($request->user()->role, [1, 2, 3]))
            $user = $user->where('school_id', $request->user()->school_id);

        if($request->term !== null)
            $user = $user->where(function($q) use ($request) {
                $q->orWhere('firstname', 'LIKE', '%'. $request->term .'%');
                $q->orWhere('lastname', 'LIKE', '%'. $request->term .'%');
                $q->orWhere(DB::raw('CONCAT(firstname, " ", lastname)'), 'LIKE', '%'. $request->term .'%');
            });

        switch ($request->filter_by) {
            case 'pending':
                $user->where('status', 0);
                break;
                
            case 'denied':
                $user->where('status', 2);
                break;

            case 'approved':
                $user->where('status', 1);
                break;

            // case 'approved-enrolled':
            //     $user->where('status', 1);
            //     $user->has('student.current_record', '!=', NULL);
            //     break;

            // case 'approved-unenrolled':
            //     $user->where('status', 1);
            //     $user->whereDoesntHave('student.current_record');
            //     break;
            
            default:
                # code...
                break;
        }

        if($request->student_id !== null
        && is_array($request->student_id)) {

            $user = $user->whereIn('id', $request->student_id);
            $user = $user->get();

        } else {
             $user = $user->paginate(10);
        }

       

        return $this->_res([
            'code' => 200,
            'data' => $user,
            'params' => $request->all()
        ]);
    }

    public function get(Request $request) {
        $v = Validator::make($request->all(), [
            'user_id' => 'required|exists:students,user_id'
        ]);
        
        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $user = User::where('role', 3)
                ->where('id', $request->user_id)
                ->with('student')
                ->with('student.classes')
                ->with('student.classes.sc')
                ->with('student.classes.sc.section')
                ->with('student.classes.sc.section.grade_level')
                ->with('student.classes.sc.subject')
                ->with('student.current_record')
                ->with('student.current_record.section')
                ->with('student.current_record.grade_level')
                ->with('student.current_record.school_year')
                ->first();

        if(!$user)
            return $this->_res([
                'code' => 404,
                'data' => [],
            ]);

        return $this->_res([
            'code' => 200,
            'data' => [
                'user' => $user,
                'current_record' => $user->student->records()
                                ->with('educational_level')
                                ->with('grade_level')
                                ->with('section')
                                ->with('school_year')
                                ->first()
            ],
        ]);
    }

    public function admit(Request $request) {
        $v = Validator::make($request->all(), [
            'school_id' => 'required|exists:schools,id',

            'picture' => 'required|mimes:jpg,png,fig,jpeg',
            'is_new' => 'required',

            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'birthday' => 'required',
            'gender' => 'required',
            'religion' => 'required',

            'street' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'province' => 'required',
            'region' => 'required',

            'last_school' => 'required',
            'last_school_year' => 'required',
            'last_school_address' => 'required',
            'last_school_graduated' => 'required',

            'guardian_firstname' => 'required',
            'guardian_lastname' => 'required',
            'guardian_relationship' => 'required',
            'guardian_occupation' => 'nullable',
            'guardian_contact' => 'required',

            'father_firstname' => 'nullable',
            'father_lastname' => 'nullable',
            'father_occupation' => 'nullable',
            'father_contact' => 'nullable',

            'mother_firstname' => 'nullable',
            'mother_lastname' => 'nullable',
            'mother_occupation' => 'nullable',
            'mother_contact' => 'nullable',
        ], [
            'is_new.required' => 'Please select if you\'re a new or old student.'
        ]);
        
        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $ext = $request->picture->extension();
        $name = $request->picture->getClientOriginalName();
        $filename  = md5($name . time()) .'.'.$ext;
        $request->picture->storeAs('avatars', $filename);

        $user = new User;
        $user->avatar = '/avatars/' . $filename;
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt('password');;
        $user->role = 3;
        $user->grant = "*";
        $user->school_id = $request->school_id;
        $user->status = 0;
        $user->save();

        // generate control number

        $student = new Student;
        $student->user_id = $user->id;
        $student->control_number = date('Y') . '-' . str_pad($user->id, 6, "0", STR_PAD_LEFT);
        $student->student_number = '-';
        $student->birthday = $request->birthday;
        $student->gender = $request->gender;
        $student->religion = $request->religion;
        $student->street = $request->street;
        $student->barangay = $request->barangay;
        $student->city = $request->city;
        $student->province = $request->province;
        $student->region = $request->region;
        $student->last_school = $request->last_school;
        $student->last_school_year = $request->last_school_year;
        $student->last_school_address = $request->last_school_address;
        $student->last_school_graduated = $request->last_school_graduated;
        $student->guardian_firstname = $request->guardian_firstname;
        $student->guardian_lastname = $request->guardian_lastname;
        $student->guardian_relationship = $request->guardian_relationship;
        $student->guardian_occupation = $request->guardian_occupation;
        $student->guardian_contact = $request->guardian_contact;
        $student->father_firstname = $request->father_firstname;
        $student->father_lastname = $request->father_lastname;
        $student->father_occupation = $request->father_occupation;
        $student->father_contact = $request->father_contact;
        $student->mother_firstname = $request->mother_firstname;
        $student->mother_lastname = $request->mother_lastname;
        $student->mother_occupation = $request->mother_occupation;
        $student->mother_contact = $request->mother_contact;
        $student->is_new = $request->is_new;
        $student->applying_for = $request->applying_for;
        $student->save();

        return $this->_res([
            'code' => 200,
            'data' => [
                'user' => $user,
                'student' => $student,
                'grade_level' => GradeLevel::where('id', $request->applying_for)
                                ->with('educational_level')
                                ->first()
            ]
        ]);
    }

    public function create(Request $request) {
        $v = Validator::make($request->all(), [
            'school_id' => 'required|exists:schools,id',

            'picture' => 'required|mimes:jpg,png,fig,jpeg',
            'is_new' => 'required',

            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'birthday' => 'required',
            'gender' => 'required',
            'religion' => 'required',

            'street' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'province' => 'required',
            'region' => 'required',

            'applying_for' => 'required|exists:grade_levels,id',

            'last_school' => 'required',
            'last_school_year' => 'required',
            'last_school_address' => 'required',
            'last_school_graduated' => 'required',

            'guardian_firstname' => 'required',
            'guardian_lastname' => 'required',
            'guardian_relationship' => 'required',
            'guardian_occupation' => 'nullable',
            'guardian_contact' => 'required',

            'father_firstname' => 'nullable',
            'father_lastname' => 'nullable',
            'father_occupation' => 'nullable',
            'father_contact' => 'nullable',

            'mother_firstname' => 'nullable',
            'mother_lastname' => 'nullable',
            'mother_occupation' => 'nullable',
            'mother_contact' => 'nullable',
        ], [
            'is_new.required' => 'Please select if you\'re a new or old student.'
        ]);
        
        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $ext = $request->picture->extension();
        $name = $request->picture->getClientOriginalName();
        $filename  = md5($name . time()) .'.'.$ext;
        $request->picture->storeAs('avatars', $filename);

        $user = new User;
        $user->avatar = '/avatars/' . $filename;
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = '-';
        $user->role = 3;
        $user->grant = "*";
        $user->school_id = $request->user()->school_id;
        $user->status = 1;
        $user->save();

        // generate control number

        $student = new Student;
        $student->user_id = $user->id;
        $student->control_number = date('Y') . '-' . str_pad($user->id, 6, "0", STR_PAD_LEFT);
        $student->student_number = '-';
        $student->birthday = $request->birthday;
        $student->gender = $request->gender;
        $student->religion = $request->religion;
        $student->street = $request->street;
        $student->barangay = $request->barangay;
        $student->city = $request->city;
        $student->province = $request->province;
        $student->region = $request->region;
        $student->last_school = $request->last_school;
        $student->last_school_year = $request->last_school_year;
        $student->last_school_address = $request->last_school_address;
        $student->last_school_graduated = $request->last_school_graduated;
        $student->guardian_firstname = $request->guardian_firstname;
        $student->guardian_lastname = $request->guardian_lastname;
        $student->guardian_relationship = $request->guardian_relationship;
        $student->guardian_occupation = $request->guardian_occupation;
        $student->guardian_contact = $request->guardian_contact;
        $student->father_firstname = $request->father_firstname;
        $student->father_lastname = $request->father_lastname;
        $student->father_occupation = $request->father_occupation;
        $student->father_contact = $request->father_contact;
        $student->mother_firstname = $request->mother_firstname;
        $student->mother_lastname = $request->mother_lastname;
        $student->mother_occupation = $request->mother_occupation;
        $student->mother_contact = $request->mother_contact;
        $student->mother_contact = $request->mother_contact;
        $student->save();

        return $this->_res([
            'code' => 200,
            'data' => [
                'user' => $user,
                'student' => $student
            ]
        ]);
    }

    
    public function update(Request $request) {
        $v = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',

            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email,' . $request->user_id,
            'birthday' => 'required',
            'gender' => 'required',

            'street' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'province' => 'required',
            'region' => 'required',

            'last_school' => 'required',
            'last_school_year' => 'required',
            'last_school_address' => 'required',
            'last_school_graduated' => 'required',

            'guardian_firstname' => 'required',
            'guardian_lastname' => 'required',
            'guardian_relationship' => 'required',
            'guardian_occupation' => 'required',
            'guardian_contact' => 'required',

            'father_firstname' => 'required',
            'father_lastname' => 'required',
            'father_occupation' => 'required',
            'father_contact' => 'required',

            'mother_firstname' => 'required',
            'mother_lastname' => 'required',
            'mother_occupation' => 'required',
            'mother_contact' => 'required',
        ]);
        
        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $user = User::where('id', $request->user_id)->where('role', 3)->first();

        if(!$user)
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => [
                    'user_id' => [
                        'The selected user id is invalid'
                    ]
                ]
            ]);


        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->grant = "*";
        $user->save();

        $student = Student::where('user_id', $user->id)->first();
        $student->user_id = $user->id;
        $student->student_number = $request->student_number ?? '-';
        $student->birthday = $request->birthday;
        $student->gender = $request->gender;
        $student->street = $request->street;
        $student->barangay = $request->barangay;
        $student->city = $request->city;
        $student->province = $request->province;
        $student->region = $request->region;
        $student->last_school = $request->last_school;
        $student->last_school_year = $request->last_school_year;
        $student->last_school_address = $request->last_school_address;
        $student->last_school_graduated = $request->last_school_graduated;
        $student->guardian_firstname = $request->guardian_firstname;
        $student->guardian_lastname = $request->guardian_lastname;
        $student->guardian_relationship = $request->guardian_relationship;
        $student->guardian_occupation = $request->guardian_occupation;
        $student->guardian_contact = $request->guardian_contact;
        $student->father_firstname = $request->father_firstname;
        $student->father_lastname = $request->father_lastname;
        $student->father_occupation = $request->father_occupation;
        $student->father_contact = $request->father_contact;
        $student->mother_firstname = $request->mother_firstname;
        $student->mother_lastname = $request->mother_lastname;
        $student->mother_occupation = $request->mother_occupation;
        $student->mother_contact = $request->mother_contact;
        $student->mother_contact = $request->mother_contact;
        $student->save();

        return $this->_res([
            'code' => 200,
            'data' => [
                'user' => $user,
                'student' => $student
            ]
        ]);
    }

    public function admission_approval(Request $request) {
        $v = Validator::make($request->all(), [
            'user_id.*' => 'required|exists:users,id',
            'status' => 'required|in:1,2',
            'reason' => 'nullable'
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $checkUser = User::with('student')
                ->has('student.current_record')
                ->whereIn('id', $request->user_id)
                ->get()
                ->count();

        if($checkUser != 0)
            return $this->_res([
                'code' => 422,
                'data' => [],
                'msg' => [
                    'status' => 'err',
                    'msg' => 'Can\'t process the approval.'
                ]
            ]);

        $user = User::whereIn('id', $request->user_id)
        ->update([
            'status' => $request->status
        ]);

        $user = User::whereIn('id', $request->user_id)
                ->get();

        if($request->reason != null
        && $request->status == 2) {
            $reject = new Rejection;
            $reject->type = 1;
            $reject->type_id = $user->id;
            $reject->message = $request->reason;
            $reject->save();
        }

        return $this->_res([
            'code' => 200,
            'data' => [
                'user' => $user,
                'reject' => $reject ?? null
            ]
        ]);
    }

    public function enrollment_regular(Request $request) {
        $v = Validator::make($request->all(), [
            'students.*' => [
                'required',
                'exists:students,user_id',
            ],
            // 'school_year' => 'required|exists:school_years,id',
            // 'educational_level_id' => 'required|exists:educational_levels,id',
            'grade_level_id' => 'required|exists:grade_levels,id',
            'section_id' => 'required|exists:sections,id',
        ]);

        try {
            return DB::transaction(function() use ($request) {
                $section = Section::find($request->section_id);
                $classes = SectionClass::where('section_id', $section->id)->get();

                $_data = [];
                
                foreach ($request->students as $key => $value) {
                    $user = User::find($value);
                    if($user->status != 1
                    && $user->role == 3) {
                         return $this->_res([
                            'code' => 422,
                            'data' => [],
                            'errors' => [
                                'students' => [
                                    'Student must be approved.'
                                ]
                            ]
                        ]);
                    }

                    if($user->student->current_record()->first() != null
                    && $user->student->current_record()->first()->status == 1)
                         return $this->_res([
                            'code' => 422,
                            'data' => [],
                            'errors' => [
                                'students' => [
                                    'Some student is currently enrolled.'
                                ]
                            ]
                        ]);

                    // Insert a Record
                    $sr = new StudentRecord;
                    $sr->user_id = $value;
                    $sr->school_year_id = SchoolYear::orderBy('id', 'DESC')->first()->id;
                    // $sr->curriculum_id = 0;
                    $sr->grade_level_id = $request->grade_level_id;
                    $sr->educational_level_id = GradeLevel::find($request->grade_level_id)->educational_level_id;
                    $sr->section_id = $request->section_id;
                    $sr->enrollment_type = 1;
                    $sr->status = 1;
                    $sr->save();

                    $_data[$key] = $sr;

                    // Insert Classes
                    $_classes = [];
                    foreach($classes as $c) {
                        $sc = new StudentClass;
                        $sc->user_id = $sr->user_id;
                        $sc->student_records_id = $sr->id;
                        $sc->section_class_id = $c->id;
                        // $sc->status = 1;
                        $sc->save();
                        
                        $_classes[] = $sc;
                    }

                    $_data[$key]['classes'] = $_classes;
                }

                return $this->_res([
                    'code' => 200,
                    'data' => $_data
                ]);

            });
        } catch(Exception $e) {
            return $this->_res([
                'code' => 500,
                'data' => [],
                'errors' => [
                    'students' => [
                        'Something went wrong.'
                    ]
                ] 
            ]);
        }
    }
    public function enrollment_irregular(Request $request) {
        $v = Validator::make($request->all(), [
            'students.*' => [
                'required',
                'exists:students,user_id',
            ],
            // 'school_year' => 'required|exists:school_years,id',
            // 'educational_level_id' => 'required|exists:educational_levels,id',
            'grade_level_id' => 'required|exists:grade_levels,id',
            'classes.*' => 'required|exists:section_classes,id',
        ]);

        try {
            return DB::transaction(function() use ($request) {
                $classes = SectionClass::whereIn('id', $request->classes)->get();
                $section = Section::find($classes[0]->section_id)->first();

                $_data = [];
                
                foreach ($request->students as $key => $value) {
                    $user = User::find($value);
                    if($user->status != 1
                    && $user->role == 3) {
                         return $this->_res([
                            'code' => 422,
                            'data' => [],
                            'errors' => [
                                'students' => [
                                    'Student must be approved.'
                                ]
                            ]
                        ]);
                    }

                    if($user->student->current_record()->first() != null
                    && $user->student->current_record()->first()->status == 1)
                         return $this->_res([
                            'code' => 422,
                            'data' => [],
                            'errors' => [
                                'students' => [
                                    'Some student is currently enrolled.'
                                ]
                            ]
                        ]);

                    // Insert a Record
                    $sr = new StudentRecord;
                    $sr->user_id = $value;
                    $sr->school_year_id = SchoolYear::orderBy('id', 'DESC')->first()->id;
                    // $sr->curriculum_id = 0;
                    $sr->grade_level_id = $request->grade_level_id;
                    $sr->educational_level_id = GradeLevel::find($request->grade_level_id)->educational_level_id;
                    $sr->section_id = $classes[0]->section_id;
                    $sr->school_year_id = $section->school_year_id;
                    $sr->enrollment_type = 2;
                    $sr->status = 1;
                    $sr->save();

                    $_data[$key] = $sr;

                    // Insert Classes
                    $_classes = [];
                    foreach($classes as $c) {
                        $sc = new StudentClass;
                        $sc->user_id = $sr->user_id;
                        $sc->student_records_id = $sr->id;
                        $sc->section_class_id = $c->id;
                        // $sc->status = 1;
                        $sc->save();
                        
                        $_classes[] = $sc;
                    }

                    $_data[$key]['classes'] = $_classes;
                }

                return $this->_res([
                    'code' => 200,
                    'data' => $_data
                ]);

            });
        } catch(Exception $e) {
            return $this->_res([
                'code' => 500,
                'data' => [],
                'errors' => [
                    'students' => [
                        'Something went wrong.'
                    ]
                ] 
            ]);
        }
    }

}
