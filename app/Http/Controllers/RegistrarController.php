<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Models\User;

class RegistrarController extends Controller
{
    public function list(Request $request) {
        $users = User::where('role', 1)
                ->with('school');

        if($request->school_id !== null) {
            $users = $users->where('school_id', $request->school_id);
        }

        if($request->status != null) 
            $users = $users->where('status', $request->status);
            
            
        $users = $users->paginate(10);

        return $this->_res([
            'code' => 200,
            'data' => $users,
        ]);
    }

    public function get(Request $request) {
        $user = User::findOrFail($request->id);

        return $this->_res([
            'code' => 200,
            'data' => $user,
        ]);
    }

    public function create(Request $request) {
        $v = Validator::make($request->all(), [
            'school_id' => 'required|exists:schools,id',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);
        
        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $filename = '';

        if($request->hasFile('picture')) {
            $ext = $request->picture->extension();
            $name = $request->picture->getClientOriginalName();
            $filename  = md5($name . time()) .'.'.$ext;
            $request->picture->storeAs('avatars', $filename);
        }
        
        $user = new User;
        $user->avatar = $filename == '' ? '' : '/avatars/' . $filename;
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->title = $request->title;
        $user->password = bcrypt($request->password);
        $user->grant = "*";
        $user->role = 1;
        $user->school_id = $request->school_id;
        $user->save();

        return $this->_res([
            'code' => 200,
            'data' => $user,
        ]);

    }

    public function update(Request $request) {
        $v = Validator::make($request->all(), [
            'id' => 'required|exists:users,id',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email,' . $request->id,
            'password' => 'nullable',
            'confirm_password' => 'nullable|same:password'
        ]);
        
        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);
        
        $user = User::where('id', $request->id)->where('role', 1)->first();

        if($request->hasFile('picture'))
            $user->avatar = $filename == '' ? '' : '/avatars/' . $filename;

        if($request->password !== null)
            $user->password = bcrypt($request->password);

        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->title = $request->title;
        $user->save();

        return $this->_res([
            'code' => 200,
            'data' => $user,
        ]);
    }
}
