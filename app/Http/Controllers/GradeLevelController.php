<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Models\GradeLevel;

class GradeLevelController extends Controller
{
    public function public_list(Request $request) {
        $v = Validator::make($request->all(), [
            'school_id' => 'required|exists:schools,id',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $grade_level = GradeLevel::with('educational_level')
                    ->where('school_id', $request->school_id)
                    ->where('status', 1)
                    ->get();

        return $this->_res([
            'code' => 200,
            'data' => $grade_level
        ]);
    }

    public function list(Request $request) {
       
        $grade_level = GradeLevel::with('educational_level')
                    ->where('school_id', $request->user()->school_id);

        if($request->status !== null)
            $grade_level = $grade_level->where('status', $request->status);
        else
            $grade_level = $grade_level->where('status', 1);
        
        $grade_level = $grade_level->get();

        return $this->_res([
            'code' => 200,
            'data' => $grade_level,
            'params' => $request->all()
        ]);
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
            'educational_level_id' => 'required|exists:educational_levels,id',
            'name' => 'required',
            'numeric' => 'required',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => $request->user(),
                'errors' => $v->errors()
            ]);
        
        $gl = new GradeLevel;
        $gl->name = $request->name;
        $gl->numeric = $request->numeric;
        $gl->educational_level_id = $request->educational_level_id;
        $gl->school_id = $request->user()->school_id;
        $gl->save();

        return $this->_res([
            'code' => 200,
            'data' => $gl,
        ]);
    }

    public function update(Request $request) {
        $v = Validator::make($request->all(), [
            'grade_level_id' => 'required|exists:grade_levels,id',
            'educational_level_id' => 'required|exists:educational_levels,id',
            'name' => 'required',
            'numeric' => 'required',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);
        
        $gl = GradeLevel::find($request->grade_level_id);
        $gl->numeric = $request->numeric;
        $gl->name = $request->name;
        $gl->educational_level_id = $request->educational_level_id;
        $gl->save();

        return $this->_res([
            'code' => 200,
            'data' => $gl,
        ]);
    }

    public function status(Request $request) {
        $v = Validator::make($request->all(), [
            'id' => 'required|exists:grade_levels,id',
            'status' => 'required|in:1,0',
        ]);

        if($v->fails())
            return $this->_res([
                'code' => 422,
                'data' => [],
                'errors' => $v->errors()
            ]);

        $gl = GradeLevel::find($request->id);
        $gl->status = $request->status;
        $gl->save();

        return $this->_res([
            'code' => 200,
            'data' => $gl,
        ]);
    }
}
