<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public $fillable = ['school_id'];
    public function grade_level() {
        return $this->belongsTo('App\Models\GradeLevel');
    }
}
