<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectionClass extends Model
{
    protected $fillable = [
        'user_id', 'subject_id', 'section_id', 'number_students'
    ];

    public function section() {
        return $this->belongsTo('App\Models\Section');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function student_class() {
        return $this->hasMany('App\Models\StudentClass');
    }

    public function subject() {
        return $this->belongsTo('App\Models\Subject');
    }

    public function announcement() {
        return $this->hasOne('App\Models\SectionClassesAnnouncement')->latest();
    }

    public function discussion() {
        return $this->belongsTo('App\Models\SectionClassesDiscussion');
    }

    public function meeting() {
        return $this->hasMany('App\Models\SectionClassesMeeting');
    }

    public function module() {
        return $this->belongsTo('App\Models\SectionClassesModule');
    }

    public function google_link() {
        return $this->hasOne('App\Models\SectionClassesGoogleLink');
    }

    public function schedule() {
        return $this->hasOne('App\Models\SectionClassesSchedule');
    }
}
