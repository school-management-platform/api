<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    // Scopes

    // Relationships
        public function school() {
            return $this->belongsTo('App\Models\School');
        }

        public function teacher() {
            return $this->hasOne('App\Models\Teacher');
        }

        public function student() {
            return $this->hasOne('App\Models\Student');
        }

        public function reject() {
            return $this->hasOne('App\Models\Rejection', 'type_id', 'id');
        }
}