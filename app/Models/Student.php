<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    public function current_record() {
        return $this->hasOne('App\Models\StudentRecord', 'user_id', 'user_id')->latest();
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function classes() {
        return $this->hasMany('App\Models\StudentClass', 'user_id', 'user_id');
    }

    public function records() {
        return $this->hasMany('App\Models\StudentRecord', 'user_id', 'user_id')->latest();
    }

    public function school_year() {
        return $this->belongsTo('App\Models\SchoolYear');
    }

    public function applying_for() {
        return $this->hasOne('App\Models\GradeLevel', 'id', 'applying_for');
    }
}
