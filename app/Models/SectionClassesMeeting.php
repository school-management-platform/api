<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectionClassesMeeting extends Model
{
    public function class() {
        return $this->belongsTo('App\Models\SectionClass');
    }
}
