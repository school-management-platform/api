<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public function classes() {
        return $this->hasMany('App\Models\SectionClass');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function grade_level() {
        return $this->belongsTo('App\Models\GradeLevel');
    }

    public function educational_level() {
        return $this->belongsTo('App\Models\EducationalLevel');
    }
}
