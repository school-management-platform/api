<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GradeLevel extends Model
{
    public function subjects() {
        return $this->hasMany('App\Models\Subject');
    }

    public function educational_level() {
        return $this->belongsTo('App\Models\EducationalLevel');
    }
}
