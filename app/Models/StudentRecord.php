<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentRecord extends Model
{
    public function educational_level() {
        return $this->belongsTo('App\Models\EducationalLevel');
    }
    
    public function grade_level() {
        return $this->belongsTo('App\Models\GradeLevel');
    }

    public function section() {
        return $this->belongsTo('App\Models\Section');
    }

    public function school_year() {
        return $this->belongsTo('App\Models\SchoolYear');
    }
}
