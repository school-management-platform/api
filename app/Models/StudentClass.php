<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model
{
    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function sc() {
        return $this->hasOne('App\Models\SectionClass', 'id', 'section_class_id');
    }
}
