<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function classes() {
        return $this->hasMany('App\Models\SectionClass', 'user_id', 'user_id');
    }

    public function advisory() {
        return $this->hasMany('App\Models\Section', 'user_id', 'user_id');
    }

    public function department() {
        return $this->belongsTo('App\Models\Department');
    }
}
