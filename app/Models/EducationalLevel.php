<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationalLevel extends Model
{
    public function grade_levels() {
        return $this->hasMany('App/Models/GradeLevel');
    }
}
