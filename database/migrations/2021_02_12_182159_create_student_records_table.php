<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_records', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('school_year_id');
            // $table->unsignedBigInteger('curriculum_id');
            $table->unsignedBigInteger('grade_level_id');
            $table->unsignedBigInteger('section_id')->nullable();
            $table->integer('enrollment_type');
            $table->integer('status')->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_year_id')->references('id')->on('school_years');
            // $table->foreign('curriculum_id')->references('id')->on('curricula');
            $table->foreign('grade_level_id')->references('id')->on('grade_levels');
            $table->foreign('section_id')->references('id')->on('sections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_records');
    }
}
