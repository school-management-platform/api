<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionClassesModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_classes_modules', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('section_class_id');
            $table->string('name');
            $table->string('path');
            $table->timestamps();

            $table->foreign('section_class_id')->references('id')->on('section_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_classes_modules');
    }
}
