<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('school_id');
            $table->string('name');
            $table->unsignedBigInteger('grade_level_id');
            $table->unsignedBigInteger('educational_level_id');
            $table->unsignedBigInteger('school_year_id');
            $table->integer('status')->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('grade_level_id')->references('id')->on('grade_levels');
            $table->foreign('educational_level_id')->references('id')->on('educational_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
