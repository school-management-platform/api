<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NullableStudentFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('guardian_occupation')->nullable()->change();
            
            $table->string('mother_firstname')->nullable()->change();
            $table->string('mother_lastname')->nullable()->change();
            $table->string('mother_occupation')->nullable()->change();
            $table->string('mother_contact')->nullable()->change();

            $table->string('father_firstname')->nullable()->change();
            $table->string('father_lastname')->nullable()->change();
            $table->string('father_occupation')->nullable()->change();
            $table->string('father_contact')->nullable()->change();

            $table->string('guardian_firstname')->nullable()->change();
            $table->string('guardian_lastname')->nullable()->change();
            $table->string('guardian_occupation')->nullable()->change();
            $table->string('guardian_contact')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
