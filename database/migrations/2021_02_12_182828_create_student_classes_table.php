<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_classes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('student_records_id');
            $table->unsignedBigInteger('section_class_id');
            $table->integer('status')->default(0);
            $table->timestamps();

            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('student_records_id')->references('id')->on('student_records');
            $table->foreign('section_class_id')->references('id')->on('section_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_classes');
    }
}
