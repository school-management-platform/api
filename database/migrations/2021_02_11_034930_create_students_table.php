<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('student_number');
            $table->date('birthday');
            $table->string('gender');
            $table->string('street');
            $table->string('barangay');
            $table->string('city');
            $table->string('province');
            $table->string('region');

            $table->string('last_school')->nullable();
            $table->string('last_school_year')->nullable();
            $table->string('last_school_address')->nullable();
            $table->integer('last_school_graduated')->nullable();

            $table->string('guardian_firstname');
            $table->string('guardian_middlename')->nullable();
            $table->string('guardian_lastname');
            $table->string('guardian_relationship');
            $table->string('guardian_occupation');
            $table->string('guardian_contact');

            $table->string('mother_firstname');
            $table->string('mother_middlename')->nullable();
            $table->string('mother_lastname');
            $table->string('mother_occupation');
            $table->string('mother_contact');

            $table->string('father_firstname');
            $table->string('father_middlename')->nullable();
            $table->string('father_lastname');
            $table->string('father_occupation');
            $table->string('father_contact');
            
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
