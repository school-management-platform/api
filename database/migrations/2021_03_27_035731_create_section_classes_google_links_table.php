<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionClassesGoogleLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_classes_google_links', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('section_class_id');
            $table->string('link');
            $table->timestamps();
            
            $table->foreign('section_class_id')->references('id')->on('section_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_classes_google_links');
    }
}
