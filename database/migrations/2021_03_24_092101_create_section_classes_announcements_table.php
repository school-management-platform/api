<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionClassesAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_classes_announcements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('section_class_id');
            $table->text('content');
            $table->integer('type')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('section_class_id')->references('id')->on('section_classes');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_classes_announcements');
    }
}
