<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionClassesDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_classes_discussions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('section_class_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('section_classes_discussion_id')->nullable();
            $table->string('message');
            $table->integer('status')->default(1);
            $table->timestamps();

            $table->foreign('section_class_id')->references('id')->on('section_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_classes_discussions');
    }
}
