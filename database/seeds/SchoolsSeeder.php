<?php

use Illuminate\Database\Seeder;

class SchoolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')
        ->insert([
           [
                'name' => 'Diocese School of Antipolo',
                'code' => 'doa',
                'logo' => '/uploads/logo/Image 23.png',
                'address' => 'Antipolo, Rizal'
           ]
        ]);
    }
}
