<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        ->insert([
            [
                'email' => 'admin@complexus.ph',
                'password' => bcrypt('password'),
                'firstname' => 'Admin',
                'middlename' => '',
                'lastname' => 'User',
                'school_id' => null,
                'role' => 1,
                'grant' => '*'
            ]
        ]);
    }
}
