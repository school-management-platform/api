<?php

use Illuminate\Database\Seeder;

class EducationalLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('educational_levels')
        ->insert([
            [
                'name' => 'Pre-School',
                'numeric' => 1
            ],[
                'name' => 'Elementary',
                'numeric' => 2
            ],[
                'name' => 'High School',
                'numeric' => 3
            ],[
                'name' => 'Senior High School',
                'numeric' => 4
            ],[
                'name' => 'College',
                'numeric' => 5
            ],[
                'name' => 'Masters Program',
                'numeric' => 6
            ],[
                'name' => 'Doctorate Program',
                'numeric' => 7
            ]
        ]);
    }
}
