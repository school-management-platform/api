<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *'); 
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');

Route::post('/auth/login', 'AuthController@login');
Route::post('/auth/login/admin', 'AuthController@admin');
Route::get('/token', 'AuthController@csrf');

// Public Student
    Route::post('/student/admit', 'StudentController@admit');


// School
    Route::prefix('school')->group(function() {
        Route::get('/list', 'SchoolController@list');
        Route::get('/get', 'SchoolController@get');
    });

// Grade Level
    Route::get('/grade_levels/list', 'GradeLevelController@public_list');

// School Year
    Route::get('/current/school_year', 'SchoolYearController@current');

Route::middleware('auth:sanctum')->group(function() {
    // School
    Route::prefix('school')->group(function() {
        Route::post('/store', 'SchoolController@store');
        Route::post('/update', 'SchoolController@update');
        Route::post('/status', 'SchoolController@status');
    });

    // School Year
    Route::prefix('school_year')->group(function() {
        Route::get('/list', 'SchoolYearController@list');
        Route::post('/store', 'SchoolYearController@store');
        Route::post('/update', 'SchoolYearController@update');
    });

    // User
    Route::prefix('user')->group(function() {
        Route::post('/status', 'UserController@status');
    });

    // Student
    Route::prefix('student')->group(function() {
        Route::get('/list', 'StudentController@list');
        Route::get('/get', 'StudentController@get');
        Route::post('/create', 'StudentController@create');
        Route::post('/update', 'StudentController@update');
        Route::post('/admission/approval', 'StudentController@admission_approval');
        Route::post('/enroll/regular', 'StudentController@enrollment_regular');
        Route::post('/enroll/irregular', 'StudentController@enrollment_irregular');
    });

    // Registrar
    Route::prefix('registrar')->group(function() {
        Route::get('/list', 'RegistrarController@list');
        Route::get('/get', 'RegistrarController@get');
        Route::post('/create', 'RegistrarController@create');
        Route::post('/update', 'RegistrarController@update');
    });

    // Department
    Route::prefix('department')->group(function() {
        Route::get('/list', 'DepartmentController@list');
        Route::post('/store', 'DepartmentController@store');
        Route::post('/update', 'DepartmentController@update');
    });

    // Teacher
    Route::prefix('teacher')->group(function() {
        Route::get('/list', 'TeacherController@list');
        Route::get('/get', 'TeacherController@get');
        Route::post('/store', 'TeacherController@store');
        Route::post('/update', 'TeacherController@update');
    });

    // Educational Level
    Route::prefix('educational_level')->group(function() {
        Route::get('/list', 'EducationalLevelController@list');
        Route::post('/store', 'EducationalLevelController@store');
        Route::post('/update', 'EducationalLevelController@update');
    });

    // Grade Level
    Route::prefix('grade_level')->group(function() {
        Route::get('/list', 'GradeLevelController@list');
        Route::post('/store', 'GradeLevelController@store');
        Route::post('/update', 'GradeLevelController@update');
        Route::post('/status', 'GradeLevelController@status');
    });

    // Subject
    Route::prefix('subject')->group(function() {
        Route::get('/list', 'SubjectController@list');
        Route::post('/store', 'SubjectController@store');
        Route::post('/update', 'SubjectController@update');
        Route::post('/status', 'SubjectController@status');
    });

    // Section
    Route::prefix('section')->group(function() {
        Route::get('/list', 'SectionController@list');
        Route::post('/store', 'SectionController@store');
        Route::get('/classes', 'SectionController@classes');

        Route::get('/class', 'SectionClassController@get_class');
        Route::get('/class/students', 'SectionClassController@get_class_students');

        Route::get('/class/virtual', 'SectionClassController@get_class_meetings');
        Route::post('/class/virtual/store', 'SectionClassController@store_class_meeting');
        Route::post('/class/virtual/update', 'SectionClassController@update_class_meeting');
        Route::post('/class/virtual/delete', 'SectionClassController@delete_class_meeting');

        Route::get('/class/assessment', 'SectionClassController@get_class_assessments');
        Route::post('/class/assessment/store', 'SectionClassController@store_class_assessment');
        Route::post('/class/assessment/update', 'SectionClassController@update_class_assessment');
        Route::post('/class/assessment/delete', 'SectionClassController@delete_class_assessment');

        Route::post('/class/announcement/store', 'SectionClassController@store_class_announcement');

        Route::post('/class/google_link/store', 'SectionClassController@store_class_google_link');

        Route::post('/class/schedule/store', 'SectionClassController@store_class_schedule');

        Route::get('/class/module', 'SectionClassController@get_class_module');
        Route::post('/class/module/store', 'SectionClassController@store_class_module');
        Route::post('/class/module/delete', 'SectionClassController@delete_class_module');

        Route::get('/class/discussion', 'SectionClassController@get_class_discussion');
        Route::post('/class/discussion/store', 'SectionClassController@store_class_discussion');
        Route::post('/class/discussion/delete', 'SectionClassController@delete_class_module');

    });

    // Courses
    Route::prefix('course')->group(function() {
        Route::get('/list', 'CourseController@list');
        Route::post('/store', 'CourseController@store');
        Route::post('/update', 'CourseController@update');
        Route::post('/delete', 'CourseController@delete');
    });

});